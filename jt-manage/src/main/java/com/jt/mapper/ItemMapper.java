package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Item;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ItemMapper extends BaseMapper<Item> {

    //注意事项: 以后写sql语句时 字段名称/表名注意大小写问题.
    @Select("SELECT * FROM tb_item  ORDER BY updated  DESC LIMIT #{startNum}, #{rows}")
    List<Item> findItemByPage(int startNum, int rows);

    void deleteItems(Long[] ids);

    void updateStatus(Long[] ids, Integer status);
}
